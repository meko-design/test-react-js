const path = require('path');
const webpack = require('webpack');
const express = require('express');
const config = require('./webpack.config');
const crypto = require('crypto');

const app = express();
const compiler = webpack(config);

const privateKey = 'b0223681fced28de0fe97e6b9cd091dd36a5b71d';
const publicKey = '298bab46381a6daaaee19aa5c8cafea5'

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/hash/:timestamp', (req, res) => {
  const concatenatedString = `${req.params.timestamp}${privateKey}${publicKey}`;
  const hash = crypto.createHash('md5').update(concatenatedString).digest('hex');
  res.send(hash);
});

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
});