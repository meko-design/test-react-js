const gulp = require('gulp');
const install = require("gulp-install");
const sass = require('gulp-sass');
const gulphelp = require('gulp-help');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const runSequence = require('run-sequence');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cssbeautify = require('gulp-cssbeautify');
// task sources files
const sourcesSass = "src/sass/*.scss";
const distsass = "public/build/partialscss/";

gulp.task('sassfile',function(){
	return gulp.src(sourcesSass)
	.pipe(sass({includepaths:[
		sourcesSass
		
	]}))
	.pipe(autoprefixer())
	.pipe(sourcemaps.init())
	.pipe(sourcemaps.write('.'))
	.pipe(minifyCSS({ keepBreaks: true }))
	.pipe(sass().on('error', sass.logError))
	.pipe(cssbeautify({ indent: '  ' }))
	.pipe(gulp.dest(distsass))
});

gulp.task('clean',function(){
	return gulp.src(distsass, {read: false})
	.pipe(clean());
	
});


