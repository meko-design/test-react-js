import React from 'react';
import PropTypes from 'prop-types';

export default class HeroDetail extends React.Component {

  static propsTypes = {
    hero: PropTypes.object.isRequired,
  }

  render() {
    console.log(this.props.hero)
    console.log(this.props.hero.urls)
    return (
      <div>
        <h2 className="namehero">{this.props.hero.name}</h2>
        {this.props.hero.urls &&
          <div className="imgHero">
              <a href={this.props.hero.urls[0].url} className="title_hero"> Universe</a>
              <a href={this.props.hero.urls[1].url} className="title_hero"> Characters</a>
              <a href={this.props.hero.urls[2].url} className="title_hero"> Comics</a>
              <a href="/" className="title_hero"> Retour accueil</a>
          </div>
        }
      </div>
    );
  }
}
