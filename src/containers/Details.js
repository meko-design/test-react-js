import React from 'react';

import { request } from '../helpers';
import HeroDetail from '../components/HeroDetail';


export default class DetailsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hero: {},
    }
  }
  componentDidMount() {
    const id = this.props.match.params.id;
    request(`http://gateway.marvel.com/v1/public/characters/${id}`)
      .then(res => {
        this.setState({ hero: res.data.data.results[0] });
      });
  }
  
  render() {
    return (
      <div>
        <HeroDetail hero={this.state.hero} />
      </div>
    )
  }
}