import React from 'react';
import { Link } from 'react-router-dom';

import { request } from '../helpers';
import TitleApp from '../components/TitleApp';

export default class DetailsContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      heroes: [],
    };
  }

  componentDidMount() {
    request(`http://gateway.marvel.com/v1/public/characters`)
      .then(res => {
        this.setState({ heroes: res.data.data.results });
        console.log(res.data.data.results)
      });
  }

  render() {
    return (
      <div>
        <TitleApp name="Hero Avengers" />
        {this.state.heroes.map(hero => (
          <div key={hero.id} className="imgHero">
            <h2 className="namehero"> {hero.name} </h2>
            <aside className="imgheroitems"><img src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`} /></aside>
            <Link to={`/details/${hero.id}`} className="title_hero">Details</Link>
          </div>

        ))}
      </div>
    )
  }
}