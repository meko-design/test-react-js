import axios from 'axios';

export const request = (url) => {
  const t = new Date().getTime();
  const publicApi = '298bab46381a6daaaee19aa5c8cafea5';
  return axios.get(`/hash/${t}`).then((res) => {
    return res.data;
  }).then( hash => (
    axios.get(`${url}?ts=${t}&apikey=${publicApi}&hash=${hash}`)
  ))
};