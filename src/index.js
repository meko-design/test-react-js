import React from 'react';
import {render}  from 'react-dom';
import { Route, Link, BrowserRouter,Switch } from 'react-router-dom';

import Home from './containers/Home';
import Details from './containers/Details';
import TitleApp from './components/TitleApp';

import styles from '../public/build/partialscss/global.css'

render((
  <BrowserRouter>
    <Switch>
      <Route path="/details/:id" component={Details} />
      <Route path="/" component={Home} />
    </Switch>
  </BrowserRouter>
),
  document.getElementById('app')
);

