import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './app'
import todoApp from './reducers/reducers'

let redux = createStore (todoApp)

let rootElement = document.getElementById('app')

render((
	<div store={redux}>
	 	<App />
	 </div>
 ),

 rootElement

)